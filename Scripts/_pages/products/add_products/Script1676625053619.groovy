import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

Mobile.waitForElementPresent(
	findTestObject('Object Repository/Products/button_buy_product'), GlobalVariable.timeout)

for (int i = 1; i <= total; i++) {
	TestObject testObject = new TestObject()
	testObject.addProperty(
		'xpath', ConditionType.EQUALS, 
		"//hierarchy/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/"
		+"android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.view.View[1]/"
		+"android.view.View[1]/android.view.View[1]/android.view.View[1]/android.view.View[2]/"
		+"android.widget.ScrollView[1]/android.view.View["+i+"]/android.view.View[1]/android.widget.Button[1]")
	Mobile.tap(testObject, GlobalVariable.timeout_fast)
}