#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@login
Feature: Login Feature
  I want to login as a user
  
  Background:
  	Given I open app
  	When I tap login icon
  	
  @invalid_login
  Scenario: I should not be able to log in with unregistered data
  	And I enter random unregistered email and password
  	And I tap login button
  	Then I check if invalid login credentials message appears

  @empty_login
  Scenario Outline: I should not be able to log in with empty data
    And I enter email <email> and password <password>
    And I tap login button
    Then I check if empty login input messages appear

    Examples: 
      | email  | password |
      | a@gmail.com | |
      | | abcd1234 |
      | a@gmail.com | |
      | | |
  