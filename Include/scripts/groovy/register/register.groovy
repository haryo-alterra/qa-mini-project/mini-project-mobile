package register
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When

import common.CommonRegisterSteps
import common.CommonLoginSteps

import common.Randomizer


class register {
	/**
	 * The step definitions below match with Katalon sample Gherkin steps
	 */
	String fullname = ""
	String email = ""
	String password = ""

	@And("I go to register page")
	def I_go_to_register_page() {
		CommonRegisterSteps.goToRegisterPage()
	}

	@And("I register with random unregistered email and password and (.*) as my full name")
	def I_register_with_random_unregistered_email_and_password(String fullname) {
		this.email = Randomizer.randomizeString(10) + "@mail.com"
		this.password = Randomizer.randomizeString(10)
		this.fullname = fullname
		CommonRegisterSteps.register(fullname, email, password)
	}

	@And("I enter email (.*), password (.*) with (.*) as my full name")
	def I_enter_email_password_fullname(String email, String password, String fullname) {
		this.email = email
		this.password = password
		this.fullname = fullname
		CommonRegisterSteps.register(fullname, email, password)
	}

	@And("I tap register")
	def I_tap_register() {
		CommonRegisterSteps.tapRegister()
	}

	@And("I register with the same email")
	def I_register_with_same_email() {
		CommonLoginSteps.goToLoginPage()
		CommonRegisterSteps.goToRegisterPage()
		CommonRegisterSteps.register(fullname, email, password)
		CommonRegisterSteps.tapRegister()
	}

	@Then("I check if empty register data messages appear")
	def I_check_if_empty_login_input_messages_appear() {
		WebUI.callTestCase(findTestCase('_pages/register/verify_empty_register'),
				[('email'): email, ('password'): password, ('fullname'): fullname],
				FailureHandling.STOP_ON_FAILURE)
	}

	@Then("I check if I can login with that newly registered account")
	def I_check_if_I_can_login_with_newly_registered_account() {
		CommonLoginSteps.goToLoginPage()
		CommonLoginSteps.login(email, password)
		CommonLoginSteps.tapLogin()
		WebUI.callTestCase(findTestCase('_pages/login/verify_successful_login'),
				null,
				FailureHandling.STOP_ON_FAILURE)
	}

	@Then("Register process should fail")
	def register_process_should_fail() {
		WebUI.callTestCase(findTestCase('_pages/register/verify_failed_register'),
				null,
				FailureHandling.STOP_ON_FAILURE)
	}
}